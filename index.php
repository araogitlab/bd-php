<html>
	<head>
	
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="view/css/bootstrap.min.css">
		
		<title>Administración de Libros</title>
		
	</head>	
	<body>
		<header>
			Bienvenido a la Administración de Libros 
		</header>
		<br>
		<table border=1>			
			<tr>
				<td><a href="view/ingresar.php">Ingresar</a></td>
			</tr>
			<tr>
				<td><a href="view/mostrar.php">Ver</a></td>
			</tr>
		</table>
		<footer>
			<br> Copyright 2019
		</footer>
		
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="view/js/jquery.min.js"></script>
		<script src="view/js/popper.min.js"></script>
		<script src="view/js/bootstrap.min.js"></script>
		
	</body>
</html>